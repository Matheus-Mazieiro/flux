using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;

public class Dodge : MonoBehaviour
{
    static List<Transform> shots = new List<Transform>();
    float distanceLimit = 500;
    float reppelForce = 10;
    public bool dodge;



    private void Update()
    {
        if(dodge)
            StartCoroutine(MyUpdate());        
    }
    // Update is called once per frame
    IEnumerator MyUpdate()
    {
        foreach (Transform t in shots)
        {
            if (t != null)
            {
                if (Vector2.Distance(transform.position, t.position) <= distanceLimit)
                {
                    Vector2 dir = (transform.position - t.position);
                    Rigidbody2D rb = GetComponent<Rigidbody2D>();
                    rb.AddForce(dir.normalized * Vector2.Distance(transform.position, t.position) * reppelForce, ForceMode2D.Impulse);
                    yield return new WaitForSeconds(2);
                    break;
                }
            }
            else shots.Remove(t);
        }
    }


    //y = ax + b
    //a -> tg �ngulo de disparo
    //b -> altura 'y' na tela
    //velocidade do tiro = 20 (* 12000?)
    //Tiro vive por 5s
    public static IEnumerator IncreaseShotData(Transform shot, float time)
    {
        shots.Add(shot);
        yield return new WaitForSeconds(time);
        shots.Remove(shot);
    }
}


//Campo magnetico
