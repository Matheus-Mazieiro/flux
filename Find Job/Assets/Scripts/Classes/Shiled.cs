using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shiled : MonoBehaviour
{
    [SerializeField] Transform player;

    Vector2 offset;
    private void Start()
    {
        offset = transform.position - player.position;
    }

    void Update()
    {
        transform.position = new Vector2(player.position.x + offset.x, player.position.y + offset.y);
    }
}
