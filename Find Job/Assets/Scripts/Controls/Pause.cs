using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField] GameObject pauseButton;
    [SerializeField] GameObject playButton;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Gaminho.Statics.WithShield = true;
            if (Time.timeScale != 0)
            {
                PauseGame(true);
            }
            else
            {
                PauseGame(false);
            }
        }
    }

    public void PauseGame(bool pause)
    {
        if (pause)
        {
            foreach (AudioSource source in GameObject.FindObjectsOfType<AudioSource>())
            {
                source.Pause();
            }
            Time.timeScale = 0;
            pauseButton.SetActive(false);
            playButton.SetActive(true);
        }
        else
        {
            foreach (AudioSource source in GameObject.FindObjectsOfType<AudioSource>())
            {
                source.UnPause();
            }
            Time.timeScale = 1;
            pauseButton.SetActive(true);
            playButton.SetActive(false);
        }
    }
}
