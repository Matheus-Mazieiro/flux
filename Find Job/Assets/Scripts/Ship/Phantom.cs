using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phantom : MonoBehaviour
{
    public float delayTime;
    Transform myTransform;
    public GameObject myMotor;
    public GameObject myShield;
    public Transform p1;
    GameObject p1Motor;
    GameObject p1Shield;
    List<Vector3> pos = new List<Vector3>();
    List<Vector3> rot = new List<Vector3>();
    List<bool> motor = new List<bool>();
    List<bool> shield = new List<bool>();

    private void Start()
    {
        GetComponent<Rigidbody2D>().gravityScale = 0.001f;
        myTransform = transform;
        p1Motor = p1.gameObject.GetComponent<ControlShip>().MotorAnimation;
        p1Shield = p1.gameObject.GetComponent<ControlShip>().Shield;
        myShield.SetActive(false);


        StartCoroutine(SlowUpdate());
        StartCoroutine(MyUpdate());
    }

    public IEnumerator MyUpdate()
    {
        yield return new WaitForSeconds(delayTime);
        while (true)
        {
            UpdatePosition();
            yield return new WaitForSeconds(0.02f);
        }
    }

    public IEnumerator SlowUpdate()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.02f);
            motor.Add(p1Motor.activeSelf);
            shield.Add(p1Shield.activeSelf);
            pos.Add(p1.position);
            rot.Add(p1.eulerAngles);
        }
    }

    public void UpdatePosition()
    {
        myTransform.position = pos[0];
        pos.RemoveAt(0);

        myTransform.eulerAngles = rot[0];
        rot.RemoveAt(0);

        if (!myMotor.activeSelf && motor[0])
            myMotor.SetActive(true);
        else if (myMotor.activeSelf && !motor[0])
            myMotor.SetActive(false);
        motor.RemoveAt(0);

        if (!myShield.activeSelf && shield[0])
            myShield.SetActive(true);
        else if (myShield.activeSelf && !shield[0])
            myShield.SetActive(false);
        shield.RemoveAt(0);
    }
}
