using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Gaminho;

public static class SaveSystem
{
    public static void SaveLevel()
    {
        string path = Application.persistentDataPath + "/level.sav";
        int level = Statics.CurrentLevel;
        using(FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine(level);
            }
        }
        Debug.Log("<b>[SaveSystem]</b> Game saved");
    }

    public static int LoadLevel()
    {
        string path = Application.persistentDataPath + "/level.sav";
        if (File.Exists(path))
        {
            int lv = 15;
            using (StreamReader sr = File.OpenText(path))
            {
                string level = sr.ReadLine();
                lv = int.Parse(level);
                Debug.Log(lv);
            }
            return lv;
        } else
        {
            Debug.LogWarning("[SaveSystem] File not found in " + path);
            return 0;
        }
    }

    public static void DeleteSavedGame()
    {
        string path = Application.persistentDataPath + "/level.sav";
        if (File.Exists(path))
        {
            File.Delete(path);
        }
    }
}